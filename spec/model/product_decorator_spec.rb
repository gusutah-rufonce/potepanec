require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let(:other_taxon) { create(:taxon) }
  let!(:product_a) { create(:product, taxons: [taxon]) }
  let!(:product_b) { create(:product, taxons: [taxon]) }
  let!(:product_c) { create(:product, taxons: [other_taxon]) }

  describe 'related_products' do
    it 'have related_products' do
      expect(product_a.related_products).to include product_b
    end

    it 'does not have unrelated_products' do
      expect(product_a.related_products).not_to include product_c
    end

    it 'product_a is not included in related_products' do
      expect(product_a.related_products).not_to include product_a
    end
  end
end
