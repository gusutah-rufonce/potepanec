require 'rails_helper'

RSpec.feature "Categories", tyep: :system do
  given(:taxonomy) { create(:taxonomy) }
  given(:bag) { create(:taxon, name: 'Bag', parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  given(:mag) { create(:taxon, name: 'Mag', parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  given!(:product_a) { create(:product, name: 'bag_1', price: 50, taxons: [bag]) }
  given!(:product_b) { create(:product, name: 'mag', price: 100, taxons: [mag]) }
  given!(:product_c) { create(:product, name: 'bag_2', price: 150, taxons: [bag]) }

  background do
    visit potepan_category_path(bag.id)
  end

  scenario 'display categories page' do
    expect(page).to have_title bag.name
    expect(page).not_to have_content product_b.name
    expect(page).not_to have_content product_b.display_price

    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content bag.name
      expect(page).to have_content mag.name
    end

    within first('.productBox') do
      expect(page).to have_content product_a.name
      expect(page).to have_content product_a.display_price
    end

    within all('.productBox').last do
      expect(page).to have_content product_c.name
      expect(page).to have_content product_c.display_price
    end
  end

  scenario 'move page from category link' do
    within '.side-nav' do
      click_on bag.name
      expect(current_path).to eq potepan_category_path(bag.id)
      click_on mag.name
      expect(current_path).to eq potepan_category_path(mag.id)
    end
  end

  scenario 'move page from product link' do
    click_on product_a.name
    expect(current_path).to eq potepan_product_path(product_a.id)

    within '.singleProduct' do
      expect(page).to have_content product_a.name
      expect(page).to have_content product_a.display_price
      expect(page).not_to have_content product_c.name
      expect(page).not_to have_content product_c.display_price
    end
  end
end
