require 'rails_helper'

RSpec.feature 'Products', type: :system do
  given(:taxon_a) { create(:taxon) }
  given(:taxon_b) { create(:taxon) }
  given!(:product_a) { create(:product, taxons: [taxon_a]) }
  given!(:related_product) { create(:product, price: 50, taxons: [taxon_a]) }
  given!(:unrelated_product) { create(:product, price: 100, taxons: [taxon_b]) }
  given!(:count_test_products) { create_list(:product, 5, taxons: [taxon_a]) }

  background do
    visit potepan_product_path(product_a.id)
  end

  scenario 'display correct products page' do
    expect(page).to have_title product_a.name
    within '.singleProduct' do
      expect(page).to have_content product_a.name
      expect(page).to have_content product_a.display_price
      expect(page).to have_content product_a.description
    end
  end

  scenario 'display related products' do
    expect(page).to have_selector '.potepan_related_products', count: 4
    within '.productsContent' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario 'does not display unrelated products' do
    within '.productsContent' do
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.display_price
    end
  end

  scenario 'go from products page to categories page' do
    click_link '一覧ページに戻る'
    expect(current_path).to eq potepan_category_path(taxon_a.id)
  end

  scenario 'click on the related product to go to the products page' do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
