require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    subject { full_title(arg) }

    context "when given argument is empty string" do
      let(:arg) { "" }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "when given argument is nil" do
      let(:arg) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "when given argument exists " do
      let(:arg) { "title" }

      it { is_expected.to eq "#{arg} - BIGBAG Store" }
    end
  end
end
