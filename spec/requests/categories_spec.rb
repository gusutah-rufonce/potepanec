require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  describe "GET #show" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "taxonomy.name" do
      expect(response.body).to include taxonomy.name
    end

    it "taxon.name" do
      expect(response.body).to include taxon.name
    end

    it "product.name" do
      expect(response.body).to include product.name
    end
  end
end
