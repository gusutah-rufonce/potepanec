require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }

  before do
    get potepan_product_path(product.id)
  end

  describe "GET #show" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "product.name" do
      expect(response.body).to include product.name
    end
  end
end
