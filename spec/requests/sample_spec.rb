require 'rails_helper'

RSpec.describe "Samples", type: :request do
  before do
    get potepan_index_path
  end

  describe "GET #index" do
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end
end
