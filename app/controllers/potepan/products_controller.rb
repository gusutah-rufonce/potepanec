class Potepan::ProductsController < ApplicationController
  DISPLAYED_PRODUCTS_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).limit(DISPLAYED_PRODUCTS_COUNT)
  end
end
